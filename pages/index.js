import Link from "next/link";
import Image from "next/image";

export default function Home() {
    return (
        <div>
            <Link href="/chanchitos">Ir a chanchitos</Link>
            <p>Chanchito feliz</p>
            <Image src="/1.jpg" width={400} height={800} />
        </div>
    );
}
